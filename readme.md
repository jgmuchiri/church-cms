##Church Content Management System

This is a church content management with online giving system build on Lavarel Framework. Please see attached installation guide.

or open a ticket https://amdtllc.com/support.


> **Note:**
> We aim to provide this application for free and provision a marketplace for developers to create their themes and plugins that can be added to the platform.

##Requirements

* PHP >5.6
* SSH access
* MySQL database
* composer
* npm

##Installation

See instructions at https://gitlab.com/jgmuchiri/church-cms/wikis/Installation

## Official Documentation

* See attached UserGuide for installation instructions


## Security Vulnerabilities

If you discover a security vulnerability within this application, please send an e-mail to safety@amdtllc.com. All security vulnerabilities will be promptly addressed.

